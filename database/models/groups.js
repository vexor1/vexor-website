const mongoose = require("mongoose");

const groupSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please Include a group name."]
    },
    desc: {
        type: String,
        required: false
    },
    icon: {
        type: String,
        required: [true, "Please include an icon."]
    }
});

// this method will hash the password before saving the user model


// this method generates an auth token for the user

const Group = mongoose.model("Group", groupSchema);
module.exports = Group;
