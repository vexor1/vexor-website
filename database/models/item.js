const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const itemSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please Include the name"]
    },
    desc: {
        type: String,
        required: false
    },
    file: {
        type: String,
        required: [true, "Please Include icon"]
    },
    category:{
        hat: false,
        shirt: false,
        pants: false,
        tools: false,
        tshirt: false,

    }

});

const item = mongoose.model("Item", itemSchema);
module.exports = item;
