const express = require("express");
const app = express();
const port = 3000;
const morgan = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
const rateLimit = require("express-rate-limit");
const handlebars = require("express-handlebars");
const

const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100 // limit each IP to 100 requests per windowMs
});

//  apply to all requests
app.use(limiter);
app.use(morgan("dev"));
app.use("/public", express.static(__dirname + "/public"));
app.use(express.static("public"));
app.use(helmet());
app.set('trust proxy', 1);



//Sets our app to use the handlebars engine
app.set("view engine", "handlebars");



//Sets handlebars configurations (we will go through them later on)
app.engine(
    "handlebars",
    handlebars({
        layoutsDir: __dirname + "/views/layouts",
    })
);




app.get("/", (req, res) => {
    //Serves the body of the page aka "main.handlebars" to the container //aka "index.handlebars"
    res.render("main", { layout: "index" });
});
app.post('API/auth/signup', (req, res) => {

});
app.post('API/auth/signin', (req, res) => {

});
app.get('/API/auth/user', (req, res) => {

});
app.get('/API/auth/admin', (req, res) => {

});
app.get('/API/auth', (req, res) => {
    res.send('API is up!') //TODO Implement API full status.
});
app.post('/API/auth', (req, res) => {
    res.json('test') //TODO add damned database response.
});
// Listen!
app.listen(port, () => console.log(`App listening to port ${port}`));