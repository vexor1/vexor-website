const {Router} = require('express');
const routes = require('express').Router();


routes.get('/dashboard', (request, response, next) => {
    console.log("authentication WIP")
    res.send("Work in progress")
})

routes.get('/', (req, res, next) => {
    console.log("main page requested.")
    res.sendFile('../pages/index.html')
})


module.exports = routes;